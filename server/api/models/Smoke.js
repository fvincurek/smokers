/**
* Smoke.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var Smoke = {
  // Enforce model schema in the case of schemaless databases
  schema: true,

  attributes: {
    location  		: { type: 'object', unique: false, defaultsTo: '' },
    address   		: { type: 'object', unique: false, defaultsTo: '' },
    country_code 	: { type: 'string', unique: false, defaultsTo: '' },
    uuid  	  		: { type: 'string', unique: false, defaultsTo: '' },
    username  	 	: { type: 'string', unique: false, defaultsTo: '' },
    gender        : { type: 'string', unique: false, defaultsTo: '' },
    datetime      : { type: 'string', unique: false, defaultsTo: '' },
    type          : { type: 'string', unique: false, defaultsTo: 'smoke' },
    user:{
      model: "user"
    }
  }
};

module.exports = Smoke;
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ngCordova'])

.run(function($rootScope, $ionicPlatform, $cordovaDevice, $http, $timeout, $cordovaGeolocation, $cordovaNetwork, $ionicPopup) {
  $ionicPlatform.ready(function() {
    
    // APP DRAWER !!! --------------------------------------------------------------------
      $rootScope.drawer = {
        state: false,
        defaults: {
            drawer: 'drawer',
            top: 56,
            contentOffset: 56,
            maxOpacity: 0.4
        },
        show: function( state ){
          if( $rootScope.drawer.state === false ){
            $rootScope.drawer.state = true;
            $('.drawer-overlay').css('visibility', 'visible');
            $('.drawer-overlay').css('opacity', '0.4');
            $( '#nav-toggle' ).addClass( 'active' );
            $( $rootScope.drawer.defaults.drawer ).css('transform', 'translate3d(0, '+$rootScope.drawer.defaults.top+'px, 0)');
          }else{
            $rootScope.drawer.hide();
          }
        },
        hide: function( state ){
            $rootScope.drawer.state = false;
            $( '#nav-toggle' ).removeClass( "active" );
            $( $rootScope.drawer.defaults.drawer ).removeClass('open');
            $( $rootScope.drawer.defaults.drawer ).css('transform', 'translate3d(-100%, '+$rootScope.drawer.defaults.top+'px, 0)');
            if( !$rootScope.plusActive ){
              $('.drawer-overlay').css('visibility', 'hidden');
              $('.drawer-overlay').css('opacity', '0');
            }
        },
        init: function(drawerClass, swipeClass, settings, callback){
          // attach hammer listener to swipe element
          var swipe = document.getElementById(swipeClass);
          var slideme = new Hammer(swipe);
          // listen on window resize event to apply proper drawer height
          var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0)
          console.log(h);

          $rootScope.drawer.defaults.height = h - $rootScope.drawer.defaults.top;
          $( $rootScope.drawer.defaults.drawer ).css('height', $rootScope.drawer.defaults.height+'px');
          
          var viewHeight = h - $rootScope.drawer.defaults.contentOffset;
          $( '.menu-content-panel' ).css('top', $rootScope.drawer.defaults.contentOffset+'px');
          $( '.menu-content-panel' ).css('height', viewHeight+'px');
          
          window.onresize = function(event) {
            var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0)
            var viewHeight = h - $rootScope.drawer.defaults.top;
            $rootScope.drawer.defaults.height = h - $rootScope.drawer.defaults.top;
            $( $rootScope.drawer.defaults.drawer ).css('height', $rootScope.drawer.defaults.height+'px');
            //$( '.view-container' ).css('top', 0);
            $( '.menu-content-panel' ).css('top', $rootScope.drawer.defaults.top+'px');
            $( '.menu-content-panel' ).css('height', viewHeight+'px');
            $rootScope.$apply();
          };

          var pressOptions = {
            time: 1
          };
          slideme.get('press').set(pressOptions);
          slideme.on('press', function(ev) {
            console.log("PRESSS DOWN");
          });

          slideme.on('pressup', function(ev) {
            console.log("PRESS UP");
            $('.drawer-overlay').css('opacity', '0.4');
            $('.drawer-overlay').addClass('active');
            $( $rootScope.drawer.defaults.drawer ).css('transform', 'translate3d(0, '+$rootScope.drawer.defaults.top+'px, 0)');
            $rootScope.drawer.show();
          });
          // listen to events...
          slideme.on("panright", function(ev) {
            $('.drawer-overlay').css('visibility', 'visible');
            var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
            var touch = ev.center.x / (w/100);
            var positionX = Math.round(-100 + touch);
            var opacity = $rootScope.drawer.defaults.maxOpacity / 100 * Math.abs(touch);
            
            if( !ev.isFinal ){
              $('.drawer-overlay').css('opacity', opacity);
              console.log( 'translate3d('+positionX+'%, '+$rootScope.drawer.defaults.top+'px, 0px)' );
              document.getElementById( $rootScope.drawer.defaults.drawer ).style.transform = 'translate3d('+positionX+'%, '+$rootScope.drawer.defaults.top+'px, 0px)';
            }else{
              $('.drawer-overlay').css('opacity', '0.4');
              $('.drawer-overlay').addClass('active');
              $( $rootScope.drawer.defaults.drawer ).css('transform', 'translate3d(0, '+$rootScope.drawer.defaults.top+'px, 0)');
              $rootScope.drawer.show();
            }
          });        }
      }
      $rootScope.drawer.init('drawer', 'slideme');
    // APP DRAWER !!! --------------------------------------------------------------------

    // get devide UUID
    $rootScope.getUUID = function(){
      try {
        $rootScope.uuid = $cordovaDevice.getUUID();
        //$rootScope.getUser();
        }
        catch(err) {
          //alert(err.message);
          $rootScope.uuid = 'notOnMobilePassE2';
      }
    }
    $rootScope.getUUID();

    // listen for Offline event
    $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
      $ionicPopup.confirm({
        title: "Internet Disconnected",
        content: "Please connect your device to the internet then press OK."
      })
      .then(function(result) {
        $rootScope.checkConnection();
      });
      $rootScope.networkInfo.offlineState = networkState;
    })

    $rootScope.checkConnection = function(){
      try {
          //
            $rootScope.networkInfo.type = $cordovaNetwork.getNetwork()
            $rootScope.isloadingText = 'Connecting on '+$rootScope.networkInfo.type;
            $rootScope.networkInfo.isOnline = $cordovaNetwork.isOnline();
            //$rootScope.networkInfo.isOffline = $cordovaNetwork.isOffline()
            var state = $rootScope.networkInfo.isOnline ? 'online' : 'offline';
            $rootScope.isloadingText = 'You are '+state+'!';
            // listen for Online event
            if($rootScope.networkInfo.type === 'none'){
              $rootScope.isloadingText = 'You are offline! Please check your internet connection and try again.';
            }
            /*
            $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
              $rootScope.getData('raw');
              $rootScope.getUser();
              $rootScope.getLocation();
            });
            */
          //
        }
        catch(err) {
          $rootScope.isloadingText = 'Can\'t connect...';
          $timeout(function(){
            $rootScope.getData('raw');
            $rootScope.getUser();
            $rootScope.getLocation();
            /*
            $rootScope.checkConnection();
            */
          }, 5000);
        }
    }
    $rootScope.checkConnection();
    
    $ionicPlatform.on('resume', function(){
      $rootScope.checkConnection();
    });
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })

  .state('app.smokes', {
    url: "/smokes",
    views: {
      'menuContent': {
        templateUrl: "templates/smokes.html",
        controller: 'SmokesCtrl'
      }
    }
  })
  .state('app.worldStats', {
    url: "/worldStats",
    views: {
      'menuContent': {
        templateUrl: "templates/worldStats.html",
        controller: 'WorldStatsCtrl'
      }
    }
  })
  .state('app.worldMap', {
    url: "/worldMap",
    views: {
      'menuContent': {
        templateUrl: "templates/worldMap.html",
        controller: 'WorldMapCtrl'
      }
    }
  })
  .state('app.info', {
    url: "/info",
    views: {
      'menuContent': {
        templateUrl: "templates/info.html",
        controller: 'InfoCtrl'
      }
    }
  })
  .state('app.settings', {
    url: "/settings",
    views: {
      'menuContent': {
        templateUrl: "templates/settings.html",
        controller: 'SettingsCtrl'
      }
    }
  })
  .state('app.personalStats', {
    url: "/personalStats",
    views: {
      'menuContent': {
        templateUrl: "templates/personalStats.html",
        controller: 'PersonalStatsCtrl'
      }
    }
  })
  /*
  */
  ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/smokes');
});

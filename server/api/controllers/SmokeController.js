/**
 * SmokeController
 *
 * @description :: Server-side logic for managing smokes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var countryISOData={af:"Afghanistan",ax:"Åland Islands",al:"Albania",dz:"Algeria",as:"American Samoa",ad:"Andorra",ao:"Angola",ai:"Anguilla",aq:"Antarctica",ag:"Antigua and Barbuda",ar:"Argentina",am:"Armenia",aw:"Aruba",au:"Australia",at:"Austria",az:"Azerbaijan",bs:"Bahamas",bh:"Bahrain",bd:"Bangladesh",bb:"Barbados",by:"Belarus",be:"Belgium",bz:"Belize",bj:"Benin",bm:"Bermuda",bt:"Bhutan",bo:"Bolivia, Plurinational State of",bq:"Bonaire, Sint Eustatius and Saba",ba:"Bosnia and Herzegovina",bw:"Botswana",bv:"Bouvet Island",br:"Brazil",io:"British Indian Ocean Territory",bn:"Brunei Darussalam",bg:"Bulgaria",bf:"Burkina Faso",bi:"Burundi",kh:"Cambodia",cm:"Cameroon",ca:"Canada",cv:"Cape Verde",ky:"Cayman Islands",cf:"Central African Republic",td:"Chad",cl:"Chile",cn:"China",cx:"Christmas Island",cc:"Cocos (Keeling) Islands",co:"Colombia",km:"Comoros",cg:"Congo",cd:"Congo, the Democratic Republic of the",ck:"Cook Islands",cr:"Costa Rica",ci:"Côte d'Ivoire",hr:"Croatia",cu:"Cuba",cw:"Curaçao",cy:"Cyprus",cz:"Czech Republic",dk:"Denmark",dj:"Djibouti",dm:"Dominica","do":"Dominican Republic",ec:"Ecuador",eg:"Egypt",sv:"El Salvador",gq:"Equatorial Guinea",er:"Eritrea",ee:"Estonia",et:"Ethiopia",fk:"Falkland Islands (Malvinas)",fo:"Faroe Islands",fj:"Fiji",fi:"Finland",fr:"France",gf:"French Guiana",pf:"French Polynesia",tf:"French Southern Territories",ga:"Gabon",gm:"Gambia",ge:"Georgia",de:"Germany",gh:"Ghana",gi:"Gibraltar",gr:"Greece",gl:"Greenland",gd:"Grenada",gp:"Guadeloupe",gu:"Guam",gt:"Guatemala",gg:"Guernsey",gn:"Guinea",gw:"Guinea-Bissau",gy:"Guyana",ht:"Haiti",hm:"Heard Island and McDonald Islands",va:"Holy See (Vatican City State)",hn:"Honduras",hk:"Hong Kong",hu:"Hungary",is:"Iceland","in":"India",id:"Indonesia",ir:"Iran, Islamic Republic of",iq:"Iraq",ie:"Ireland",im:"Isle of Man",il:"Israel",it:"Italy",jm:"Jamaica",jp:"Japan",je:"Jersey",jo:"Jordan",kz:"Kazakhstan",ke:"Kenya",ki:"Kiribati",kp:"Korea, Democratic People's Republic of",kr:"Korea, Republic of",kw:"Kuwait",kg:"Kyrgyzstan",la:"Lao People's Democratic Republic",lv:"Latvia",lb:"Lebanon",ls:"Lesotho",lr:"Liberia",ly:"Libya",li:"Liechtenstein",lt:"Lithuania",lu:"Luxembourg",mo:"Macao",mk:"Macedonia, the former Yugoslav Republic of",mg:"Madagascar",mw:"Malawi",my:"Malaysia",mv:"Maldives",ml:"Mali",mt:"Malta",mh:"Marshall Islands",mq:"Martinique",mr:"Mauritania",mu:"Mauritius",yt:"Mayotte",mx:"Mexico",fm:"Micronesia, Federated States of",md:"Moldova, Republic of",mc:"Monaco",mn:"Mongolia",me:"Montenegro",ms:"Montserrat",ma:"Morocco",mz:"Mozambique",mm:"Myanmar",na:"Namibia",nr:"Nauru",np:"Nepal",nl:"Netherlands",nc:"New Caledonia",nz:"New Zealand",ni:"Nicaragua",ne:"Niger",ng:"Nigeria",nu:"Niue",nf:"Norfolk Island",mp:"Northern Mariana Islands",no:"Norway",om:"Oman",pk:"Pakistan",pw:"Palau",ps:"Palestine, State of",pa:"Panama",pg:"Papua New Guinea",py:"Paraguay",pe:"Peru",ph:"Philippines",pn:"Pitcairn",pl:"Poland",pt:"Portugal",pr:"Puerto Rico",qa:"Qatar",re:"Réunion",ro:"Romania",ru:"Russian Federation",rw:"Rwanda",bl:"Saint Barthélemy",sh:"Saint Helena, Ascension and Tristan da Cunha",kn:"Saint Kitts and Nevis",lc:"Saint Lucia",mf:"Saint Martin (French part)",pm:"Saint Pierre and Miquelon",vc:"Saint Vincent and the Grenadines",ws:"Samoa",sm:"San Marino",st:"Sao Tome and Principe",sa:"Saudi Arabia",sn:"Senegal",rs:"Serbia",sc:"Seychelles",sl:"Sierra Leone",sg:"Singapore",sx:"Sint Maarten (Dutch part)",sk:"Slovakia",si:"Slovenia",sb:"Solomon Islands",so:"Somalia",za:"South Africa",gs:"South Georgia and the South Sandwich Islands",ss:"South Sudan",es:"Spain",lk:"Sri Lanka",sd:"Sudan",sr:"Suriname",sj:"Svalbard and Jan Mayen",sz:"Swaziland",se:"Sweden",ch:"Switzerland",sy:"Syrian Arab Republic",tw:"Taiwan, Province of China",tj:"Tajikistan",tz:"Tanzania, United Republic of",th:"Thailand",tl:"Timor-Leste",tg:"Togo",tk:"Tokelau",to:"Tonga",tt:"Trinidad and Tobago",tn:"Tunisia",tr:"Turkey",tm:"Turkmenistan",tc:"Turks and Caicos Islands",tv:"Tuvalu",ug:"Uganda",ua:"Ukraine",ae:"United Arab Emirates",gb:"United Kingdom",us:"United States",um:"United States Minor Outlying Islands",uy:"Uruguay",uz:"Uzbekistan",vu:"Vanuatu",ve:"Venezuela, Bolivarian Republic of",vn:"Viet Nam",vg:"Virgin Islands, British",vi:"Virgin Islands, U.S.",wf:"Wallis and Futuna",eh:"Western Sahara",ye:"Yemen",zm:"Zambia",zw:"Zimbabwe"};

var getOverall = function( data ){
  var complete = [];
  _.each( countryISOData, function( value, key ){
    var hodnota = _.size( _.where( data, { country_code: key+'' } ) );
    if( hodnota > 0 ){
      complete.push(hodnota);
    }
  });
  return complete;
}

// calculate percentage
var roundPrc = function(input, helper){
  input = parseInt(input);
  helper = parseInt(helper);
  if (!input){ input = 0; };
  if (!helper){ helper = 0; };
  //var rounded = Math.round((input / (helper+input))*100, 1);
  var rounded = Math.round((input / helper)*100, 1);
  return rounded
}
// #

// Calculate color
var calculateColor = function( p, overall ){
  var complete = overall;
  var min = Math.min.apply(null, complete),
      max = Math.max.apply(null, complete);
  var c0 = p < (max/2) ? '#639a85' : '#EEcc44';
  var c1 = p < (max/2) ? '#EEcc44' : '#dc7f54';
  p = roundPrc(p, max) / 100;
  var n=p<0?p*-1:p,u=Math.round,w=parseInt;
  if(c0.length>7){
    var f=c0.split(","),t=(c1?c1:p<0?"rgb(0,0,0)":"rgb(255,255,255)").split(","),R=w(f[0].slice(4)),G=w(f[1]),B=w(f[2]);
    return "rgb("+(u((w(t[0].slice(4))-R)*n)+R)+","+(u((w(t[1])-G)*n)+G)+","+(u((w(t[2])-B)*n)+B)+")"
  }else{
    var f=w(c0.slice(1),16),t=w((c1?c1:p<0?"#000000":"#FFFFFF").slice(1),16),R1=f>>16,G1=f>>8&0x00FF,B1=f&0x0000FF;
    return "#"+(0x1000000+(u(((t>>16)-R1)*n)+R1)*0x10000+(u(((t>>8&0x00FF)-G1)*n)+G1)*0x100+(u(((t&0x0000FF)-B1)*n)+B1)).toString(16).slice(1)
  }
};
// #

module.exports = {
	fetch: function(req,res){
		Smoke.find().exec(function(err, smokes) {
		  if (err) {
	      res.send(500, { error: "DB Error" });
		  } else {
	      if (smokes.length > 0) {
	      	
	      	// calculate data for PIE chart
		      	var columns = [];
			      var colors = [];
			      var overall = getOverall( smokes );
			      var RAW_data = [];
			      _.each( countryISOData, function( value, key ){
			        var hodnota = _.size( _.where( smokes, { country_code: key+'' } ) );
			        var males = _.size( _.where( smokes, { country_code: key+'', gender: 'male' } ) );
			        var females = _.size( _.where( smokes, { country_code: key+'', gender: 'female' } ) );
			        var others = _.size( _.where( smokes, { country_code: key+'', gender: 'other' } ) );
			        if( hodnota > 0 ){
			          var color = calculateColor( hodnota, overall );
			          colors.push( color );
			          var oneData = [ value, hodnota ];
			          columns.push( oneData );
			          var raw = {
			          	iso: key,
			          	value: hodnota,
			          	name: value,
			          	male: males,
			          	female: females,
			          	other: others
			          }
			          RAW_data.push( raw );
			        }
			      });
		      // #
		      
		      var returnData = {
		      	raw: RAW_data,
		        pieChart: {
		        	data: columns,
		        	colors: colors
		      	},
		      	overall: overall,
		      	last: smokes.slice(0, 40)
		      };

       		res.json( returnData );   
	      } else {
          res.json({
					  empty: 'No entries yet!'
					});
	      }
		  }
		});
		/*
		return res.json({
		  todo: 'Not implemented yet!'
		});
		*/
	}
};


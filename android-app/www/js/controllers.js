var app = angular.module('starter.controllers',  ['ionic', 'ngCordova']);

app.controller('AppCtrl', function(
  $rootScope, $scope, $http, $timeout, $interval, 
  $cordovaGeolocation, $ionicPlatform, $cordovaDevice, $cordovaNetwork, $ionicPopup) {
  //$rootScope.getUser();
  $rootScope.isloadingStart = true;
  $rootScope.isloadingText = 'Loading...';

  $rootScope.address = $rootScope.address || {};

  $rootScope.createDisabled = false;
  $rootScope.networkInfo = [];
  $rootScope.worldStatsExists = false;
  $rootScope.worldMapExists = false;
  $rootScope.pieExists = false;
  $rootScope.personalStatsExists = false;
  $rootScope.lastNumber = 10;
  $rootScope.lastNumberUser = 10;
  $rootScope.countryCodes = countryISOData;
  $rootScope.currencyList = currencyList;
  $rootScope.isoToCurrency = isoToCurrency;

  $rootScope.getData = function( type ){
    
    if( type === 'raw' ){

      io.socket.get('/smoke/fetch', function( data ){
        $rootScope.globalData = data;
        $rootScope.$apply();
        console.log( data )
      });
      
      console.log('getting RAW data');
      io.socket.get('/smoke/?limit=0&sort=createdAt+desc', function( smokes ){
        //console.log( smokes );
        $rootScope.smokes = smokes;
        $rootScope.renderSmokes = $rootScope.smokes.slice(0, $rootScope.lastNumber);
        $rootScope.isloadingStart = false;
        $rootScope.$apply();
      });
    
    }else{

      if( !$rootScope.smokes ){
        $timeout(function(){
          $rootScope.getData(type);
        }, 500);
      }else{
        if( type === 'pie' ){
          $rootScope.createPie( $rootScope.smokes );
          $rootScope.pieExists = true;
        }else if( type === 'world' ){
          $rootScope.createWorldStats( $rootScope.smokes );
          $rootScope.worldStatsExists = true;
        }else if( type === 'world-map' ){
          $rootScope.createWorldMap( $rootScope.smokes );
          $rootScope.worldMapExists = true;
        }else if( type === 'personal' ){
          $rootScope.createPastChart( $rootScope.smokes );  
          $rootScope.personalStatsExists = true;
        }
      }

    }
  }
  $rootScope.addSmoke = function( type ){
    type = type || 'smoke';
    if( !$rootScope.createDisabled ){
      var dateTime = Date.today().toString('d-MMM-yyyy'); 
      var newSmoke = {
        location: $rootScope.location,
        address: $rootScope.address,
        country_code: $rootScope.address.country_code,
        uuid: $rootScope.uuid,
        username: $rootScope.user.name,
        gender: $rootScope.user.gender,
        datetime: dateTime, 
        user: $rootScope.user.id,
        type: type
      }
      if( $rootScope.address.loading ){
        Materialize.toast('Please wait for location!', 2000) // 4000 is the duration of the toast
      }else{
        io.socket.post('/smoke/create', newSmoke, function( result ){
          //
          $rootScope.smokes.unshift( result );
          $rootScope.renderSmokes = $rootScope.smokes.slice(0, $rootScope.lastNumber);

          $rootScope.$apply();
          
          if( $rootScope.pieExists === true ) $rootScope.updatePie( result );
          if( $rootScope.worldStatsExists === true ) $rootScope.updateWorldStats( $rootScope.smokes );
          if( $rootScope.worldMapExists === true ) $rootScope.updateWorldMap( $rootScope.smokes );
          if( $rootScope.personalStatsExists === true ) $rootScope.updatePastChart( $rootScope.smokes );
          
          $rootScope.calculatePersonalStats( $rootScope.smokes );
          $rootScope.calculateWorldStats( $rootScope.smokes );
          
          //console.log( result );
          Materialize.toast('Saved!', 2000) // 4000 is the duration of the toast
          
          $rootScope.createDisabled = true;
          $timeout(function(){
            $rootScope.createDisabled = false;
          },30000);

          $rootScope.$apply();
          //
        });  
      }
    }else{
      Materialize.toast('You can\'t smoke that quick! ;)', 3000) // 4000 is the duration of the toast
      Materialize.toast('Please wait a while', 5000) // 4000 is the duration of the toast
    }
  }
  var deleteSmokeTimeout;
  $rootScope.cancelDelete = function( smoke ){
    
    console.log( 'Cancelling deletion');
    clearTimeout(deleteSmokeTimeout);
    io.socket.post('/smoke/create', smoke, function( result ){
      //
      console.log( result );
      $rootScope.smokes = $scope.smokesBackup;
      $rootScope.renderSmokes = $rootScope.smokes.slice(0, $rootScope.lastNumber);
      if( $rootScope.pieExists === true ) $rootScope.updatePie( $rootScope.smokes );
      if( $rootScope.worldMapExists === true ) $rootScope.updateWorldMap( $rootScope.smokes );
      if( $rootScope.worldStatsExists === true ) $rootScope.updateWorldStats( $rootScope.smokes );           
      if( $rootScope.personalStatsExists === true ) $rootScope.updatePastChart( $rootScope.smokes );
      $rootScope.calculatePersonalStats( $rootScope.smokes );
      $rootScope.calculateWorldStats( $rootScope.smokes );
      $rootScope.$apply();
      //
      Materialize.toast('<span>Delete canceled</span>', 1000);
    });
  }
  $rootScope.deleteSmoke = function( delSmoke ){
    $scope.smokesBackup = angular.copy( $rootScope.smokes );

    io.socket.post('/smoke/destroy/' + delSmoke.id, function( result ){
    //console.log( result );
      Materialize.toast(
      '<span class="title">Item Deleted</span><a class="delete-toast" class="btn-flat yellow-text"><i class="mdi-content-undo"></i>Undo<a>', 
      2000,
      'toast delete');

      $('.delete-toast').on('click', function(){
        $scope.cancelDelete( result );
      });
      $rootScope.$apply();
      //
    });

    $rootScope.smokes.splice($rootScope.smokes.indexOf( delSmoke ), 1);
    $rootScope.renderSmokes = $rootScope.smokes.slice(0, $rootScope.lastNumber);

    if( $rootScope.pieExists === true ) $rootScope.updatePie( $rootScope.smokes );
    if( $rootScope.worldMapExists === true ) $rootScope.updateWorldMap( $rootScope.smokes );
    if( $rootScope.worldStatsExists === true ) $rootScope.updateWorldStats( $rootScope.smokes );
    if( $rootScope.personalStatsExists === true ) $rootScope.updatePastChart( $rootScope.smokes );
    
    $rootScope.calculatePersonalStats( $rootScope.smokes );
    $rootScope.calculateWorldStats( $rootScope.smokes );

    $rootScope.createDisabled = false;
    //
    //alert("Deleted") 
  };

  $rootScope.togglePlus = function( hide ){
    console.log( $rootScope.plusActive );
    
    if( !$rootScope.plusActive && !hide ){
      $rootScope.plusActive = true;
      $('.add-panel > .switch').addClass('active');
      $('.drawer-overlay').css('visibility', 'visible');
      $('.drawer-overlay').css('opacity', '0.4');
    }else{
      if( !$rootScope.drawer.state ){
        $('.drawer-overlay').css('visibility', 'hidden');
        $('.drawer-overlay').css('opacity', '0');
      } 
      $rootScope.plusActive = false;
      $('.add-panel > .switch').removeClass('active');
      //$('.add-panel').css('z-index', '10');
    }
  }
  $rootScope.updateTitle = function( newTitle ){
    console.log( newTitle );
    $rootScope.activeSection = newTitle;
    //$rootScope.$apply();
  }
  //
  $rootScope.getOverall = function( data ){
    var complete = [];
    _.each( $rootScope.countryCodes, function( value, key ){
      var hodnota = _.size( _.where( data, { country_code: key+'' } ) );
      if( hodnota > 0 ){
        complete.push(hodnota);
      }
    });

    var toReturn = {
      complete: complete
    }

    return toReturn;
  }

  // ---------------------------------------------------------------------- #Geolocation
  $rootScope.getLocation = function(){
    $rootScope.isloadingStart = false;
    $rootScope.address = { city: 'Please wait, locating...', loading: true, icon: 'mdi-communication-location-off' };
    var posOptions = {maximumAge: 360000, timeout: 10000, enableHighAccuracy: false};
    $cordovaGeolocation
      .getCurrentPosition(posOptions)
      .then(function (position) {
        var lat  = position.coords.latitude
        var long = position.coords.longitude
        $rootScope.location = { lat: lat, long: long };
        $rootScope.getAddress( $rootScope.location );
      }, function(err) {
        $rootScope.address = { city: 'Retrying...', loading: true, icon: 'mdi-communication-location-off' };
        $timeout(function(){
          console.log('retrying location');
          $rootScope.getLocation();
        },5000);
    });
  }
  $rootScope.getAddress = function( location ){
    $rootScope.isloadingText = 'Getting address...';
    $rootScope.address = { city: 'Please wait, getting address...', loading: true, icon: 'mdi-communication-location-off' };
    
    var internalAddress = $rootScope.offlineLocation.get_country( location.lat, location.long );
    console.log( internalAddress );
    if( !_.isEmpty( internalAddress ) ){
      $rootScope.address = {
        city: 'Unknown',
        country: internalAddress.name,
        country_code: internalAddress.iso2,
        icon: 'mdi-communication-location-on'
      }    
    };

    var url = 'http://nominatim.openstreetmap.org/reverse'
    $http.get(url, { 
      params: { 
        format: 'json', 
        lat: location.lat, 
        lon: location.long,
        zoom: '18',
        addressdetails: '1'
      } 
    }).success(function(data) {
        //alert('$rootScope.getAddress Done!');
        $rootScope.address = data.address;
        $rootScope.address.icon = 'mdi-communication-location-on';
        //$rootScope.$apply();
        console.log($rootScope.address);
    }).error(function(data) {
        $rootScope.address = { city: 'Retrying', loading: true, icon: 'mdi-communication-location-on' };
        //$rootScope.$apply();
        $timeout(function(){
          $rootScope.getAddress( location );
        },1500);
    });
  }
  // OFFLINE LOCATION
    $rootScope.offlineLocation = {};
    $rootScope.offlineLocation.my = {};
    $rootScope.offlineLocation.my.country_data = countries_data;
  // Functions
    $rootScope.offlineLocation.point_in_polygon = function(polygon, point) {
      var nvert = polygon.length;
      var c = false;
      var i = 0;
      var j = 0;
      for(i = 0, j = nvert-1; i < nvert; j = i++) {
        if( ((polygon[i][1] > point[1]) != (polygon[j][1] > point[1])) && 
           (point[0] < (polygon[j][0] - polygon[i][0]) * (point[1] - polygon[i][1]) / (polygon[j][1] - polygon[i][1]) + polygon[i][0]) ) {
          c = !c;
        }
      }
      return c;
    };
    $rootScope.offlineLocation.get_country = function(lat, lng) {
      if(typeof lat !== 'number' || typeof lng!== 'number') {
        return new Error('Wrong coordinates (' + lat + ',' + lng + ')');
      }

      var point = [lng, lat];
      var i = 0;
      var found = false;
      do {
        var country = $rootScope.offlineLocation.my.country_data[i];
        if(country.geometry.type === 'Polygon') {
          found = $rootScope.offlineLocation.point_in_polygon(country.geometry.coordinates[0], point);
        }
        else if(country.geometry.type === 'MultiPolygon') {
          var j = 0;
          do {
            found = $rootScope.offlineLocation.point_in_polygon(country.geometry.coordinates[j][0], point);
            j++;
          } while (j < country.geometry.coordinates.length && !found);
        }
        i++;
      } while (i < $rootScope.offlineLocation.my.country_data.length && !found);

      if(found) {
        return {
          code: $rootScope.offlineLocation.my.country_data[i-1].id,
          name: $rootScope.offlineLocation.my.country_data[i-1].properties.name,
          iso2: $rootScope.offlineLocation.my.country_data[i-1].iso2
        };
      }
      else {
        return null;
      }
    };
  // # OFFLINE LOCATION
  // ---------------------------------------------------------------------- #Geolocation

  $rootScope.currency = function(){
    if( !_.isUndefined( $rootScope.address ) && !_.isUndefined( $rootScope.address.country_code ) ){
      var countryCode = $rootScope.isoToCurrency[$rootScope.address.country_code.toUpperCase()];
      var currencySymbol = $rootScope.currencyList[countryCode].symbol_native;
      return currencySymbol;
    }else{
      return '--';
    }
  }
  $rootScope.getUser = function(){
    console.log( $rootScope.uuid );
    var loginUser = {
      email: $rootScope.uuid,
      password: $rootScope.uuid
    }
    io.socket.post('/auth/login', loginUser, function( user ){
      if( user.error ){
        alert( user.error );
        console.log( user );
      }
      $rootScope.user = user;
      $rootScope.user.pack_price = parseInt(user.pack_price);
      $rootScope.user.pack_value = parseInt(user.pack_value);
      $rootScope.getData('personal', false);
      $rootScope.$apply();
      console.log( $rootScope.user )
    });
  }
  $rootScope.showGravatar = function( email ){
    if( email ){
      return 'http://www.gravatar.com/avatar/' + md5(email);
    }else{
      return 'http://www.gravatar.com/avatar/';
    }
  }
  //

  ////////////////////////////////////////////////////////////////////////////////////////
  // Chart bits \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\////
  ////////////////////////////////////////////////////////////////////////////////////////
    
  // PERSONAL stats
    $rootScope.personalPastChart = [];
    $rootScope.createPastChart = function( data ){
      var chartData = $rootScope.calculatePersonalStats( data );
      console.log( chartData );
      $rootScope.personalPastChart.options = {
        chart: {
          renderTo: 'past-chart',
          type: 'areaspline'
        },
        title: {
          text: ''
        },
        subtitle: {
          text: ''
        },
        xAxis: {
          categories: chartData.names,
          crosshair: true
        },
        yAxis: {
          min: 0,
          title: {
            text: ''
          }
        },
        tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y}</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
        },
        plotOptions: {
          column: {
            pointPadding: 0.2,
            borderWidth: 0
          },
          area: {
            stacking: 'normal'
          }
        },
        series: [{
          name: 'World',
          color: '#a8cfca',
          data: chartData.dataWorld,
          dashStyle: "Dot",
          marker: {
            enabled: false,
            lineWidth: 2,
            lineColor: '#639a85'
          }
        },{
          name: 'Country',
          color: '#99bbaa',
          data: chartData.dataCountry,
          dashStyle: "Dot",
          marker: {
            enabled: false,
            lineWidth: 2
          }
        },{
          name: 'You',
          color: '#dc7f54',
          data: chartData.data,
          marker: {
            enabled: false,
            lineWidth: 3,
            lineColor: '#dc7f54'
          }
        }]
      }
      $rootScope.personalPastChart = new Highcharts.Chart( $rootScope.personalPastChart.options );
    }
    $rootScope.updatePastChart = function( data, days ){
      var newPastData = $rootScope.calculatePersonalStats( data, days );

      $rootScope.personalPastChart.axes[0].categories = newPastData.names;
      $rootScope.personalPastChart.series[0].setData(newPastData.dataWorld, true);
      $rootScope.personalPastChart.series[1].setData(newPastData.dataCountry, true);
      $rootScope.personalPastChart.series[2].setData(newPastData.data, true);
      
      console.log( $rootScope.personalPastChart );
    }
    $rootScope.calculateDay = function( data, days ){
      $rootScope.howManyDays = days;
      //
      var daysData = [],
          daysNames = [],
          daysDataCountry = [],
          daysDataWorld = [];

      for( var i = days; i >= 0; i-- ){
        var today = new Date.today();
        var day = today.add(-i).day().toString('d-MMM-yyyy');
        var dayName = new Date.parse( day ).toString('d.M');
        daysNames.push( dayName );
        var dayDataCountry = _.where( data, { datetime: day, country_code: $rootScope.address.country_code } );
        var dayDataWorld = _.where( data, { datetime: day } );
        var dayData = _.where( data, { datetime: day, uuid: $rootScope.uuid } );
        daysData.push( _.size( dayData ) );
        daysDataCountry.push( _.size( dayDataCountry ) );
        daysDataWorld.push( _.size( dayDataWorld ) );
      }
      var complete = Math.round( _.reduce(daysData, function(memo, num){ return memo + num; }, 0) / days );
      $rootScope.userAverage = complete;
      console.log( $rootScope.userAverage );
      return { daysData: daysData, daysDataCountry: daysDataCountry, daysNames: daysNames, daysDataWorld: daysDataWorld };
    }
    $rootScope.calculatePersonalStats = function( data, days ){
      days = !days ? 7 : days;
      console.log( days );
      //
        var overall = $rootScope.getOverall( data ).complete;
        var totalData = _.where( data, { uuid: $rootScope.uuid } );
        $rootScope.totalDataUser = totalData;

        var total = parseInt( _.size( totalData ) );
        var pack_value = parseInt( $rootScope.user.pack_value );
        var pack_price = parseInt( $rootScope.user.pack_price );
        var deleno = (total/pack_value)!==Infinity ? (total/pack_value) : 0; 
        var money = total > 0 ? ( deleno * pack_price ).toFixed(2) : 0;
        
        var thisDay = Date.today().toString('d-MMM-yyyy');
        var todayData = _.where( data, { datetime: thisDay, uuid: $rootScope.uuid });
        var todayDataCount = parseInt( _.size( todayData ) );

        var deleno = (todayDataCount/pack_value)!==Infinity ? (todayDataCount/pack_value) : 0; 
        var todayMoney = todayDataCount > 0 ? ( deleno * pack_price ).toFixed(2) : 0.00;
        var finalData = $rootScope.calculateDay( data, days )
      //
      var viewData = {
        money: money,
        total: total,
        today: todayDataCount,
        todayMoney: todayMoney,
        names: finalData.daysNames,
        data: finalData.daysData,
        dataCountry: finalData.daysDataCountry,
        dataWorld: finalData.daysDataWorld
      };

      $rootScope.personal = {}
      $rootScope.personal.total = viewData.total;
      $rootScope.personal.money = viewData.money;
      $rootScope.personal.today = viewData.today;
      $rootScope.personal.todayMoney = viewData.todayMoney;
      $rootScope.renderUserSmokes = totalData.slice( 0, $rootScope.lastNumberUser );
      return viewData;
    }
    $rootScope.changedValue=function(item){
      //
      $rootScope.updatePastChart( $rootScope.smokes, item );
    }
  // #
    
  // PIE chart
    $rootScope.pieChart = [];
    $rootScope.createPie = function( data ){
      var pieData = $rootScope.calculatePie( data );
      console.log( pieData.data );
      $rootScope.pieChart.options = {
        chart: {
          renderTo: 'pie-chart',
          plotBackgroundColor: null,
          plotBorderWidth: 0,
          plotShadow: false
        },
        title: {
          text: '',
          align: 'center',
          verticalAlign: 'middle',
          y: 50
        },
        tooltip: {
          pointFormat: '{series.name}: <b>{point.y} ({point.percentage:.1f}%)</b>'
        },
        plotOptions: {
          pie: {
            dataLabels: {
              enabled: false,
              distance: -50,
              style: {
                fontWeight: 'lighter',
                color: '#333',
                textShadow: '0px 1px 2px rgba(255,255,255,0.4)'
              }
            },
            startAngle: -90,
            endAngle: 90,
            center: ['50%', '100%']
          }
        },
        series: [{
          type: 'pie',
          name: 'Smoked',
          innerSize: '50%',
          data: pieData.data
        }],
        colors: pieData.colors
      };
      $rootScope.pieChart = new Highcharts.Chart( $rootScope.pieChart.options );
    };
    $rootScope.calculatePie = function( data ){
      /*  
      var columns = [];
      var colors = [];
      var overall = $rootScope.getOverall( data ).complete;
      _.each( $rootScope.countryCodes, function( value, key ){
        var hodnota = _.size( _.where( data, { country_code: key+'' } ) );
        if( hodnota > 0 ){
          var color = $rootScope.calculateColor( hodnota, overall );
          colors.push( color );
          var oneData = [ value, hodnota ];
          columns.push( oneData );
        }
      });
      var returnData = {
        data: columns,
        colors: colors
      };
      */
      return $rootScope.globalData.pieChart;
    };
    $rootScope.updatePie = function( data ){
      console.log( data );
      console.log( $rootScope.globalData.pieChart );
      /*
      var newPieData = $rootScope.calculatePie( data );
      $rootScope.pieChart.series[0].setData(newPieData.data, true);
      */
    };
  // #

  // WORLD stats
    $rootScope.worldStatsChart = [];
    $rootScope.worldGlobal = [];
    $rootScope.createWorldStats = function( data ){
      var worldData = $rootScope.calculateWorldStats( data );
      console.log( worldData );
      $rootScope.worldStatsChart.options = {
        chart: {
          renderTo: 'world-stats-chart',
          plotBackgroundColor: null,
          plotBorderWidth: 0,
          plotShadow: false,
          type: 'pie'
        },
        title: {
          text: 'Global overview'
        },
        subtitle: {
          text: 'All countries'
        },
        xAxis: {
          categories: worldData.iso,
          title: {
            text: null
          }
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Cigarettes',
            align: 'high'
          },
          labels: {
            overflow: 'justify'
          }
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'top',
          x: -40,
          y: 100,
          floating: true,
          borderWidth: 1,
          backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
          shadow: true
        },
        credits: {
          enabled: false
        },
        series: [{
          name: 'Cigarettes',
          data: worldData.pack
        }],
        colors: worldData.colors
      };
      $rootScope.worldStatsChart = new Highcharts.Chart( $rootScope.worldStatsChart.options );
    };
    $rootScope.calculateWorldStats = function( data ){
      var hodnoty = [],
          colors = [],
          countries = [],
          isos = [],
          pack = [],
          overall = $rootScope.getOverall( data ).complete;
      _.each( $rootScope.countryCodes, function( value, key ){
        var hodnota = _.size( _.where( data, { country_code: key+'' } ) );
        key = key.toUpperCase();
        if( hodnota > 0 ){
          var color = $rootScope.calculateColor( hodnota, overall );
          countries.push( value );
          hodnoty.push( hodnota );
          colors.push( color );
          isos.push( key );
          pack.push( [ value, hodnota ] );
        }
      });

      $rootScope.worldGlobal.male = _.size( _.where( data, { gender: 'male' } ) );
      $rootScope.worldGlobal.female = _.size( _.where( data, { gender: 'female' } ) );
      $rootScope.worldGlobal.other = _.size( _.where( data, { gender: '' } ) );
      $rootScope.worldGlobal.total = $rootScope.worldGlobal.other + $rootScope.worldGlobal.male + $rootScope.worldGlobal.female;

      var returnData = {
        countries: countries,
        iso: isos,
        colors: colors,
        values: hodnoty,
        pack: pack,
        global: _.size( data )
      }
      //console.log( returnData );
      return returnData;
    };
    $rootScope.updateWorldStats = function( data ){
      var newStatsData = $rootScope.calculateWorldStats( data );
      $rootScope.worldStatsChart.series[0].setData(newStatsData.pack, true);
    };
  // #
  
  // WORLD MAP chart
    $rootScope.worldMapChart = [];
    $rootScope.createWorldMap = function( data ){
      var mapData = $rootScope.calculateWorldMap( data );
      console.log( mapData );
      $rootScope.worldMapChart.options = {
        chart: {
          renderTo: 'world-map-chart'
        },
        title : {
          text : 'Zoom in on country by double click'
        },
        mapNavigation: {
          enabled: true,
          enableDoubleClickZoomTo: true
        },
        colorAxis: {
          min: 1,
          type: 'logarithmic',
          minColor: '#638885',
          maxColor: '#dc7f54'
        },
        series : [{
          data : mapData,
          mapData: Highcharts.maps['custom/world'],
          joinBy: ['iso-a2', 'code'],
          name: 'Smoked',
          tooltip: {
            valueSuffix: ''
          }
        }]
      }
      console.log( $rootScope.worldMapChart.options );
      $rootScope.worldMapChart = new Highcharts.Map( $rootScope.worldMapChart.options );
      // #map end
      // add buler event to reset pie chart back to totals
    }
    $rootScope.calculateWorldMap = function( data ){
      var hodnoty = [],
          colors = [];
      var overall = $rootScope.getOverall( data ).complete;
      _.each( $rootScope.countryCodes, function( value, key ){
        var hodnota = _.size( _.where( data, { country_code: key+'' } ) );
        key = key.toUpperCase();
        if( hodnota > 0 ){
          var color = $rootScope.calculateColor( hodnota, overall );
          var item = {
            code: key,
            value: hodnota,
            color: color,
            name: value
          }
          hodnoty.push( item );
        }
      });
      return hodnoty;
    }
    $rootScope.updateWorldMap = function( data ){
      var newMapData = $rootScope.calculateWorldMap( data );
    }
  // #
  
  ////////////////////////////////////////////////////////////////////////////////////////
  // Chart bits //////////////////////////////////////////////////////////////////////\///
  ////////////////////////////////////////////////////////////////////////////////////////

  // Format sails date
    $rootScope.formatDate = function( toformat ){
      toformat = toformat.slice(0,10).split("-").join('/');
      var thisDate = new Date.parse( toformat );
      toformat = thisDate.toString("dd.M. yyyy");
      return { date: thisDate, parsed: toformat };
    }
  // #

  // calculate percentage
    /**
      * Calculates the percentage of the given number in full number
      * eg. input =  100
      *     helper = 200
      *     return = 33 (% of 300)
      * @param  {number} input  the number of which you want the x%
      * @param  {number} helper to get the whole number - 100%
      * @return {number}        x% of full number
      */
    $rootScope.roundPrc = function(input, helper){
      input = parseInt(input);
      helper = parseInt(helper);
      if (!input){ input = 0; };
      if (!helper){ helper = 0; };
      //var rounded = Math.round((input / (helper+input))*100, 1);
      var rounded = Math.round((input / helper)*100, 1);
      return rounded
    }
  // #

  // Calculate color
    $rootScope.calculateColor = function( p, overall ){
      var complete = overall;
      var min = Math.min.apply(null, complete),
          max = Math.max.apply(null, complete);
      var c0 = p < (max/2) ? '#639a85' : '#EEcc44';
      var c1 = p < (max/2) ? '#EEcc44' : '#dc7f54';
      p = $rootScope.roundPrc(p, max) / 100;
      var n=p<0?p*-1:p,u=Math.round,w=parseInt;
      if(c0.length>7){
        var f=c0.split(","),t=(c1?c1:p<0?"rgb(0,0,0)":"rgb(255,255,255)").split(","),R=w(f[0].slice(4)),G=w(f[1]),B=w(f[2]);
        return "rgb("+(u((w(t[0].slice(4))-R)*n)+R)+","+(u((w(t[1])-G)*n)+G)+","+(u((w(t[2])-B)*n)+B)+")"
      }else{
        var f=w(c0.slice(1),16),t=w((c1?c1:p<0?"#000000":"#FFFFFF").slice(1),16),R1=f>>16,G1=f>>8&0x00FF,B1=f&0x0000FF;
        return "#"+(0x1000000+(u(((t>>16)-R1)*n)+R1)*0x10000+(u(((t>>8&0x00FF)-G1)*n)+G1)*0x100+(u(((t&0x0000FF)-B1)*n)+B1)).toString(16).slice(1)
      }
    };
  // #

  // Listen to socket events
    $rootScope.listen = function(){
      //
      ////////////////////////////////////////////////////////////////////////////////////////
      // Socket stuff ////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////////////////
      //
      io.socket.on('smoke', function( smoke ){
        console.log( smoke );
        switch ( smoke.verb ) {
        case 'created':
          //
          console.log( smoke.data );
          $rootScope.smokes.unshift( smoke.data );
          $rootScope.renderSmokes = _.uniq( $rootScope.smokes.slice(0, $rootScope.lastNumber), true );
          $rootScope.$apply();
          if( $rootScope.pieExists === true ) $rootScope.updatePie( $rootScope.smokes );
          if( $rootScope.worldMapExists === true ) $rootScope.updateWorldMap( $rootScope.smokes );
          if( $rootScope.worldStatsExists === true ) $rootScope.updateWorldStats( $rootScope.smokes );
          if( $rootScope.personalStatsExists === true ) $rootScope.updatePastChart( $rootScope.smokes );
          break;
        case 'destroyed':
          //
          var odds = _.reject($rootScope.smokes, function(value, key){ return value.id === smoke.id; });
          $rootScope.smokes = odds;
          $rootScope.renderSmokes = _.uniq( $rootScope.smokes.slice(0, $rootScope.lastNumber), true );
          $rootScope.$apply();
          if( $rootScope.pieExists === true ) $rootScope.updatePie( $rootScope.smokes );
          if( $rootScope.worldMapExists === true ) $rootScope.updateWorldMap( $rootScope.smokes );
          if( $rootScope.worldStatsExists === true ) $rootScope.updateWorldStats( $rootScope.smokes );
          if( $rootScope.personalStatsExists === true ) $rootScope.updatePastChart( $rootScope.smokes );  
          break;
        case 'updated':
          //
          //console.log( 'updated', smoke );
          break;
        }

        $rootScope.$apply();
      });
      //
    };
    $rootScope.listen();
  // #

});

////////////////////////////////////////////////////////////////////////////////////////
// Smokes view controller //////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
app.controller('SmokesCtrl', function($rootScope, $scope, $cordovaDevice, $ionicPopup) {
  
  $rootScope.updateTitle( 'Smokes' );
  $rootScope.getData('pie');

  $scope.loadMore = function(){
    $rootScope.renderSmokes = $rootScope.smokes.slice(0, $rootScope.lastNumber+10);
    $rootScope.lastNumber += 10;
  };

  $rootScope.checkGender = function( gender ){
    return gender === 'other' ? 'planet' : gender;
  };

  $rootScope.formatTime = function( toformat ){
    toformat = new Date.parse( toformat );
    //console.log( toformat );
    toformat = toformat.toString("HH:mm:ss");
    //console.log( toformat );
    return toformat;
  }

  $rootScope.getGlobalAddress = function( country_code ){
    //
    return $rootScope.countryCodes[country_code];
  };

  $rootScope.isOwner = function( uuid ){
    //
    return uuid === $rootScope.uuid;
  };
});

////////////////////////////////////////////////////////////////////////////////////////
// World view controller ///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
app.controller('WorldStatsCtrl', function($rootScope, $scope) {

  $rootScope.updateTitle( 'World Stats' );
  $rootScope.getData('world');
});

////////////////////////////////////////////////////////////////////////////////////////
// World view controller ///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
app.controller('WorldMapCtrl', function($rootScope, $scope) {

  $rootScope.updateTitle( 'World Map' );
  $rootScope.getData('world-map');
});

////////////////////////////////////////////////////////////////////////////////////////
// Personal stats controller ///////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
app.controller('PersonalStatsCtrl', function($rootScope, $scope) {

  $rootScope.updateTitle( 'Personal Stats' );
  $rootScope.getData('personal');
  $scope.loadMore = function(){
    $rootScope.renderUserSmokes = $rootScope.totalDataUser.slice(0, $rootScope.lastNumberUser+10);
    $rootScope.lastNumberUser += 10;
  };
});

////////////////////////////////////////////////////////////////////////////////////////
// Info controller /////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
app.controller('InfoCtrl', function($rootScope, $scope) {
  //
  $rootScope.updateTitle( 'Info' );
});

////////////////////////////////////////////////////////////////////////////////////////
// Settings controller /////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
app.controller('SettingsCtrl', function($rootScope, $scope, $timeout) {

  $(document).ready(function() {
    $('select').material_select();
  });
  $rootScope.updateTitle( 'Settings' );
  var uuid = $rootScope.uuid || 'guest';
  $scope.userBackup = $rootScope.user;
  $scope.update = function(user) {
    $scope.userBackup = angular.copy(user);
    console.log( $rootScope.user );
    console.log( user );

    var updateData = {
      gender: user.gender,
      name: user.name,
      pack_price: user.pack_price,
      pack_value: user.pack_value,
      email: user.email,
      show_gravatar: user.show_gravatar
    }

    console.log( updateData );
    io.socket.post('/user/update/'+$rootScope.user.id, updateData, function( result ){
      
      console.log( result );
      $rootScope.user = result;
      $rootScope.user.pack_price = parseInt(result.pack_price);
      $rootScope.user.pack_value = parseInt(result.pack_value);
      if(user.show_gravatar && _.isEmpty(user.email)){
        $rootScope.user.gravatarUrl = $rootScope.showGravatar( user.email );
      }
      $rootScope.calculatePersonalStats( $rootScope.smokes );
      $rootScope.$apply();
      Materialize.toast('<span><i class="mdi-action-done"></i>Settings saved</span>', 2000, 'toast done');
    });
  };
  $scope.reset = function() {
    $scope.user = angular.copy($scope.userBackup);
  };
});
var app = angular.module('starter.controllers',  []);

app.controller('AppCtrl', function( $rootScope, $scope) {
  io.socket.get('/smoke/?limit=0', function(smokes){
    $rootScope.smokes = smokes;
    $rootScope.$apply();
  });
  console.log('AppCtrl');

});

app.controller('SmokesCtrl', function( $rootScope, $scope) {
  
  console.log('SmokesCtrl');

});

app.filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});
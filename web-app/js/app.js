// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', [])

.run(function($rootScope) {
  console.log('run');
  
  var heatMap;
  var heatmapLayer = null;
  var heatMapMarkers = [];

  $rootScope.isoNames = countryISOData;
  
  ////////////////////////////////////////////////////////////////////////////////////////
  // Socket stuff ////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////

  io.socket.on('smoke', function( smoke ){
    //console.log( smoke );
    switch ( smoke.verb ) {
    case 'created':
      //
      $rootScope.smokes.push( smoke.data );
      break;
    case 'destroyed':
      //
      var odds = _.reject($rootScope.smokes, function(value, key){ return value.id === smoke.id; });
      $rootScope.smokes = odds;
      break;
    case 'updated':
      //
      break;
    }
    $rootScope.$apply();
    $rootScope.updateMap( $rootScope.smokes );
  });
  //
  io.socket.on('disconnect', function( smoke ){
    //alert('Disconnect');
    $rootScope.disconnected = true;
    $rootScope.$apply();
  });
  //
  io.socket.on('connect', function( smoke ){
    //alert('Disconnect');
    io.socket.get('/smoke/?limit=0', function( smokes ){
      $rootScope.smokes = smokes;
      $rootScope.$apply();
      $rootScope.createMap( smokes );
    });
  });

  window.onresize = function(event) {
    var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
    var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0)
    console.log( w, h );
    $rootScope.legendHeight = h - 76;
    $rootScope.$apply();
  };

  $rootScope.calculateMap = function( data ){
    var heatmapData = [],
        perCountry = [];
    _.each( data, function( value, key ){
      //console.log( value );
      var item = {
        lat: value.location.lat || 0,
        lng: value.location.long || 0,
        count: 1
      }
      if( item.lat !== 0 || item.lng !== 0 ) heatmapData.push( item );
    });

    _.each( $rootScope.isoNames, function( value, key ){
      var country = _.size( _.where( data, { country_code: key+'' } ) );

      if( country > 0 ){

        var entries = _.where( data, { country_code: key+'' } );
        //console.log( entries );
        //console.log( _.flatten( entries ) );
        var locations = _.map( _.flatten( entries ), _.iteratee('location'));
        var lats = _.map( locations, _.iteratee('lat'));
        var lngs = _.map( locations, _.iteratee('long'));
        //console.log( lats, lngs );
        var latsSum = parseInt(_.reduce(lats, function(memo, num){ return parseInt(memo) + parseInt(num); }, 0)/country);
        var lngsSum = parseInt(_.reduce(lngs, function(memo, num){ return parseInt(memo) + parseInt(num); }, 0)/country);

        perCountry.push( { 
          key: country,
          name: value,
          latLng: [latsSum,lngsSum] 
        } ); 
      }
    });

    var returnData = {
      heatmap: heatmapData,
      perCountry: perCountry
    }
    
    $rootScope.heatmapLegend = returnData;
    $rootScope.legend = perCountry;
    $rootScope.$apply();
    console.log( $rootScope.legend );
    return returnData;
  }
  $rootScope.createMap = function( data ){
    
    var heatmapData = $rootScope.calculateMap( data );
    var size = _.size( heatmapData.heatmap );

    var finalData = {
      max: size,
      data: heatmapData.heatmap
    };
    console.log( finalData );
    var CartoDB_DarkMatter = L.tileLayer('http://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
      subdomains: 'smokers',
      maxZoom: 19
    });
    var cfg = {
      // radius should be small ONLY if scaleRadius is true (or small radius is intended)
      // if scaleRadius is false it will be the constant radius used in pixels
      //"radius": 0.0025,
      "radius": 25,
      "maxOpacity": .5, 
      // scales the radius based on map zoom
      "scaleRadius": false, 
      // if set to false the heatmap uses the global maximum for colorization
      // if activated: uses the data maximum within the current map boundaries 
      //   (there will always be a red spot with useLocalExtremas true)
      "useLocalExtrema": true,
      // which field name in your data represents the latitude - default "lat"
      latField: 'lat',
      // which field name in your data represents the longitude - default "lng"
      lngField: 'lng',
      // which field name in your data represents the data value - default "value"
      valueField: 'count',

      gradient: {
        // enter n keys between 0 and 1 here
        // for gradient color customization
        '.2': '#638885',
        '.5': '#99aa33',
        '.98': '#dc7f54'
      }
    };

    heatmapLayer = new HeatmapOverlay(cfg);
    heatMap = new L.Map('map-canvas', {
      center: new L.LatLng(25.6586, -80.3568),
      zoom: 3,
      layers: [CartoDB_DarkMatter, heatmapLayer]
    });
    /*
    heatMap.locate({setView: true, maxZoom: 6});
    function onLocationFound(e) {
      var radius = e.accuracy * 10;

      L.marker(e.latlng).addTo(heatMap)
        .bindPopup("You are somwhere here").openPopup();

      //L.circle(e.latlng, e.accuracy).addTo(heatMap);
    }
    heatMap.on('locationfound', onLocationFound);
    */
    heatmapLayer.setData( finalData );
    
    _.each( heatmapData.perCountry, function( value, key ){
      var marker = L.marker(value.latLng);
      marker.addTo(heatMap).bindPopup('Smoked '+value.key+' cigarettes - '+value.name);
      console.log('add marker: '+marker._leaflet_id );
      heatMapMarkers[marker._leaflet_id] = marker;
    });
    
    heatMap.on('zoomend', function(type, target) {
      var size = (cfg.radius * (type.target._animateToZoom/5)).toFixed(5);

      var nuConfig = {
        radius: size/*,
        maxOpacity: .5,
        minOpacity: 0,
        blur: .75*/
      };
      //console.log( nuConfig ); // e is an event object (MouseEvent in this case)
      //heatmapLayer.configure( nuConfig ); 
      //heatmapLayer.heatmap.set("radius",size);
    });
  }
  $rootScope.updateMap = function( data ){
    
    var heatmapData = $rootScope.calculateMap( data );
    var size = _.size( heatmapData.heatmap );

    var finalData = {
      max: size,
      data: heatmapData.heatmap
    };
    
    console.log( heatMapMarkers );

    _.each( heatMapMarkers, function( value, key ){

      if( !_.isUndefined(value) ){

        console.log('remove marker: '+value._leaflet_id );
        heatMap.removeLayer(heatMapMarkers[value._leaflet_id]);
        
      }

    });
    
    heatmapLayer.setData( finalData );
    _.each( heatmapData.perCountry, function( value, key ){
      console.log('add marker: '+value._leaflet_id );
      console.log( value._leaflet_id );

      var marker = L.marker(value.latLng);
      marker.addTo(heatMap).bindPopup('Smoked '+value.key+' cigarettes - '+value.name);
      
      heatMapMarkers[marker._leaflet_id] = marker;
    });
  }

});

/**
 * User
 *
 * @module      :: Model
 * @description :: This is the base user model
 * @docs        :: http://waterlock.ninja/documentation
 */

module.exports = {

	// Enforce model schema in the case of schemaless databases
  schema: true,
  attributes: require('waterlock').models.user.attributes({

  	uuid  			  : { type: 'string', unique: false, defaultsTo: '' },
    name    			: { type: 'string', unique: false, defaultsTo: '' },
    gender 	  		: { type: 'string', unique: false, defaultsTo: '' },
    pack_price  	: { type: 'string', unique: false, defaultsTo: '0' },
    pack_value	  : { type: 'string', unique: false, defaultsTo: '0' },
    email         : { type: 'string', unique: false, defaultsTo: '' },
    show_gravatar : { type: 'boolean', unique: false, defaultsTo: false },
    
    jsonWebTokens: {
      collection: 'jwt',
      via: 'owner'
    },
    smokes:{
      collection: "smoke",
      via: "user"
    }

  }),
  
  beforeCreate: require('waterlock').models.user.beforeCreate,
  beforeUpdate: require('waterlock').models.user.beforeUpdate
};

# Smokers #

Smokers is a application to gather data about smoking habits worldwide.

### What it can do?

* Shows you summary of your smoked cigarettes
* total number for your country
* smoked money

### How do I get set up?

TODO

### Download

[![Smokers on app store](https://developer.android.com/images/brand/en_generic_rgb_wo_45.png)](https://play.google.com/store/apps/details?id=com.ionicframework.smokersapp402381)

### Licence

[![CC](https://i.creativecommons.org/l/by-nc/4.0/80x15.png)](http://creativecommons.org/licenses/by-nc/4.0/)

This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/)